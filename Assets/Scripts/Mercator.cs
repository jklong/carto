﻿using System;


/*
 * <summary> Provides transformations between spherical and cartesian coordinates
 * using the Mercator projection. The returned coordinates are centered around the equator at meridian 0
 * and truncated symmetrically at the given latitude.
 * </summary>
 * 
 */
public class Mercator
{
    float LAMBDA_0 = 0.0f; //Lambda_0 for the mercator transform.

    double truncationLatitude;
    double projectionRadius;
    float height;
    float width;

    /**
     * <summary> Construct a projection onto a map of <c>width</c> x <c>height</c> pixels, 
     * with the projection truncated at <c>truncationLatitude</c> degrees.</summary>
     * 
     * <param name="height">the height of the map, in px</param>
     * <param name="width">the width of the map, in px</param>
     * <param name="truncationLatitude">the latitude at which to symmetrically truncate the projection, in degrees. Must be between 0 and 90.</param>
     */
    public Mercator(float width, float height, double truncationLatitude)
    {
        if (truncationLatitude < 0 || truncationLatitude >= 90)
        {
            throw new ArgumentException("Invalid truncation latitude");
        }

        if (width < 0)
        {
            throw new ArgumentException("Negative map width given");
        }

        if (height < 0)
        {
            throw new ArgumentException("Negative map height given");
        }

        this.width = width;
        this.height = height;
        this.truncationLatitude = (Math.PI * truncationLatitude) / 180d; //Convert to radians
        this.projectionRadius = this.width / (2.0d * Math.PI); // Radius of our projection in px.
    }

    /**
     * <summary> Convert a cartesian x,y pair into a spherical latitude,longitude 
     * pair.</summary>
     *  
     * <param name = "c">the cartesian coordinates to be transformed</param>
     * 
     * <returns>the latitude and longitude corresponding to the cartesian point</returns>
     */
    public Spherical ToSpherical(Cartesian c)
    {
        //Basic sanity checking
        if (c.x < (-width / 2) || c.y > (width / 2))
        {
            throw new ArgumentOutOfRangeException("x component out of range");
        }

        if (c.y < (-height / 2) || c.y > (height / 2))
        {
            throw new ArgumentOutOfRangeException("y component out of range");
        }

        Spherical s = new Spherical { };

        s.longitude = this.LAMBDA_0 + (c.x / this.projectionRadius);
        s.latitude = 2 * Math.Atan(Math.Exp(c.y / this.projectionRadius)) - (Math.PI / 2.0d);

        return s;

    }

    public Cartesian ToCartesian(Spherical s)
    {
        //Basic sanity checks
        if (s.latitude > (Math.PI/2.0f) || s.latitude < (-Math.PI / 2.0f))
        {
            throw new ArgumentOutOfRangeException("latitude invalid");
        }

        if (s.longitude > Math.PI/2.0f || s.longitude < (-Math.PI / 2.0f))
        {
            throw new ArgumentOutOfRangeException("longitude invalid");
        }

        Cartesian c = new Cartesian { };

        double phi = s.latitude;
        double lambda = s.longitude;

        c.x = (int)Math.Round(this.projectionRadius * (lambda - this.LAMBDA_0));
        c.y = (int)Math.Round(this.projectionRadius * Math.Log(Math.Tan((Math.PI / 4.0d) + (phi / 2.0f))));

        return c;
    }
}

