﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public IcosahedronSphere icosphere;
    public Slider globalProgressBar;
    public Slider resolution; 
    public TMPro.TextMeshProUGUI resolutionLabel;
    public Slider smallScaleDeform; 
    public TMPro.TextMeshProUGUI smallScaleDeformLabel;
    public TMPro.TextMeshProUGUI caption;
    
    public void update_resolution()
    {
        icosphere.resolution = (int)resolution.value;
        resolutionLabel.text = ((int)resolution.value).ToString();
    }

    public void update_small_scale_deform()
    {
        icosphere.smallScaleDeform = smallScaleDeform.value;
        smallScaleDeformLabel.text = System.Math.Round(smallScaleDeform.value, 3).ToString();
    }

}
