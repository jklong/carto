﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Threading;
using UnityEngine.UI;

public class IcosahedronSphere : MonoBehaviour
{
    [Range(0,15)]
    public int resolution = 1;
    public float radius = 1.0f;

    [Range(0,0.1f)]
    // The fraction in either direction that we can deform each vector3 along it's length for small scale randomness
    public float smallScaleDeform = 0.1f;

    [HideInInspector]
    public bool needsUpdate = false;

    private Mesh m;
    private Vector3[] vertices;
    private int[] tris;

    private int r = 0;

    private bool isGenerating = false;
    private volatile int progressValue;
    private Mutex progressMutex = new Mutex(false);
    private Slider progress;
    private TMPro.TextMeshProUGUI caption;

    public void Generate()
    {
        m = gameObject.GetComponent<MeshFilter>().sharedMesh;
        vertices = m.vertices;
        tris = m.triangles;

        isGenerating = true;
        GenerateIcosahedronSphere();
        Debug.Log("Exiting Generate()");

    }

    public void Update()
    {
        if (gameObject.activeSelf && !isGenerating)
        {
            gameObject.transform.rotation = Quaternion.Euler(0f, 0.5f * (r++), 0f);
        }
        
        if (r > 360) {
            r = 0;
        }

        if (needsUpdate)
        {
            RecalculateMesh();
        }

        if (isGenerating)
        {
            progressMutex.WaitOne();
            progress.value = progressValue;
            progressMutex.ReleaseMutex();
        }
    }
    /**
     * <summary> Generates a subdivided icosahedron sphere with <c>radius</c> and a subdivision factor of <c>resolution<c>, centered around the origin.</summary>
     */
    private void GenerateIcosahedronSphere()
    {
        progress = FindObjectOfType<UIManager>().globalProgressBar;
        caption = FindObjectOfType<UIManager>().caption;
        m.Clear();
        GenerateIcosahedron(radius);

        caption.text = "Generating sphere";
        progress.value = 0;
        progress.maxValue = 3 * 20 * ((int)Math.Pow(2d,2d * resolution));

        StartCoroutine(SubdivideIcosahedronAsync(() => {
            caption.text = "Deforming";
            progress.maxValue = vertices.Length;
            // Do async deform
            StartCoroutine(AsyncDeform(() =>
            {
                this.needsUpdate = true;
                this.isGenerating = false;
                progressValue = 0;
                progress.maxValue = 0;
                caption.text = "";
            }));
        }));
    }

    /**
     * <summary> Generate a standard 12-vertex icosahedron with a radius of <c>r</c></summary>
     * 
     * <param name="r">Radius of the icosahedron</param>
     */ 
    public void GenerateIcosahedron(float r)
    {
        float phi = (1 + Mathf.Sqrt(5)) / 2.0f; // Golden ratio

        float a = (r / 0.951056f) * 2.0f; //2 * edge length, from r

        //12 vertices of an icosahedron
        Vector3[] verts = { 
           new Vector3(0,  a,  a * phi),
           new Vector3(0, -a,  a * phi),
           new Vector3(0,  a, -a * phi),
           new Vector3(0, -a, -a * phi),

           new Vector3( a * phi, 0,  a),
           new Vector3( a * phi, 0, -a),
           new Vector3(-a * phi, 0,  a),
           new Vector3(-a * phi, 0, -a),

           new Vector3( a,  a * phi, 0),
           new Vector3(-a,  a * phi, 0),
           new Vector3( a, -a * phi, 0),
           new Vector3(-a, -a * phi, 0)
        };

        // 20 faces
        int[] triangles = {
            0, 6, 1,
            0, 9, 6,
            0, 8, 9,
            0, 4, 8,
            0, 1, 4,
            1, 6, 11,
            6, 9, 7,
            9, 8, 2,
            8, 4, 5,
            4, 1, 10,
            2, 8, 5,
            5, 4, 10,
            10, 1, 11,
            11, 6, 7,
            7, 9, 2,
            3, 7, 2,
            3, 2, 5,
            3, 5, 10,
            3, 10, 11,
            3, 11, 7
        };

        vertices = verts;
        tris = triangles;
    }

    private IEnumerator SubdivideIcosahedronAsync(Action callback)
    {

        Thread t = new Thread(new ThreadStart(DoAsyncSubdivide));
        t.Start();

        float start = Time.realtimeSinceStartup;
        yield return new WaitUntil(() => t.ThreadState == ThreadState.Stopped);
        Debug.Log($"Subdivide complete in {Time.realtimeSinceStartup - start}");
        callback();
    }

    private IEnumerator AsyncDeform(Action callback)
    {
        Thread t = new Thread(new ThreadStart(Deform));
        t.Start();

        float start = Time.realtimeSinceStartup;
        yield return new WaitUntil(() => t.ThreadState == ThreadState.Stopped);
        Debug.Log($"Deform Complete in {Time.realtimeSinceStartup - start}");

        callback();
    }

    private void DoAsyncSubdivide() {

        int T = (int)Math.Pow(2d,2d * resolution);
        int nextTri = 0;
        int nextVertex = 0;
        Hashtable vertIndex = new Hashtable(); //An index for a Vector3 to its index in the new vertices array.

        Vector3[] originalVerts = this.vertices;
        int[] originalTris = this.tris;
        
        tris = new int[3 * 20 * T];
        vertices = new Vector3[10 * T + 2];

        UnityEngine.Profiling.Profiler.BeginSample("IcosahedronSphere.SubdivideIcosahedron");
        for (int i = 0; i < originalTris.Length; i += 3)
        {
            Vector3 a, b, c;

            a = originalVerts[originalTris[i]];
            b = originalVerts[originalTris[i + 1]];
            c = originalVerts[originalTris[i + 2]];

            // Calculate vertexes resulting from subdivision along edges.
            Vector3[] intervalsAB = CalculateIntervals(a, b, resolution + 1);
            Vector3[] intervalsBC = CalculateIntervals(b, c, resolution + 1);
            Vector3[] intervalsAC = CalculateIntervals(a, c, resolution + 1);

            // Add the vertexes to our list if they're not there already

            List<Vector3> newV3s = new List<Vector3>();
            newV3s.AddRange(intervalsAB);
            newV3s.AddRange(intervalsAC);
            newV3s.AddRange(intervalsBC);

            foreach (Vector3 v in newV3s)
            {
                GetOrAddToIndex(ref vertIndex, ref vertices, v, ref nextVertex);
            }

            // Add T new triangles using vertex2 indexes

            for (int j = 1; j < intervalsAC.Length; j++)
            {
                // Number of divisions on this row: j - 1
                // Number of edges = j

                // A vector representing a step of a single edge along the plane BC
                Vector3 r = intervalsAB[j] - intervalsAC[j];
                r = r / j;

                for (int k = 0; k < j; k++)
                {
                    // For every edge, we form an upward pointing triangle.
                    // This goes to the point one step "up" the AC edge and k along the BC edge

                    //                                       p
                    //                                     /   \
                    // Define the points of a triangle as s --> t

                    Vector3 s = intervalsAC[j] + (r * k);
                    Vector3 t = s + r;

                    Vector3 pointUp = new Vector3();
                    pointUp = intervalsAC[j - 1] + (r * k);

                    //Project these points to the sphere
                    s = s / (s.magnitude / vertices[0].magnitude);
                    t = t / (t.magnitude / vertices[0].magnitude);
                    pointUp = pointUp / (pointUp.magnitude / vertices[0].magnitude);

                    tris[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices, s, ref nextVertex);
                    tris[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices, pointUp, ref nextVertex);
                    tris[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices, t, ref nextVertex);

                    // For all but the last row, do down triangles too
                    if (j < intervalsAB.Length - 1)
                    {
                        Vector3 pointDown = intervalsAC[j + 1] + (r * (k + 1));
                        pointDown = pointDown / (pointDown.magnitude / vertices[0].magnitude);
                        tris[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices, s, ref nextVertex);
                        tris[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices, t, ref nextVertex);
                        tris[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices, pointDown, ref nextVertex);
                    }

                    progressMutex.WaitOne();
                    progressValue = nextTri;
                    progressMutex.ReleaseMutex();
                }
            }
        }

        UnityEngine.Profiling.Profiler.EndSample();
    }
/**
     * <summary> Subdivide an icosahedron into an icosasphere at the given resolution.</summary>
     * <param name="resolution">Number of iterations of subdivison.</param>
     */
    private void SubdivideIcosahedron(int resolution)
    {

        int T = (int)Math.Pow(2d,2d * resolution);
        int[] tris2 = new int[3 * 20 * T];
        int nextTri = 0;
        Vector3[] vertices2 = new Vector3[10 * T + 2];
        int nextVertex = 0;
        Hashtable vertIndex = new Hashtable(); //An index for a Vector3 to its index in the new vertices array.

        UnityEngine.Profiling.Profiler.BeginSample("IcosahedronSphere.SubdivideIcosahedron");

        for (int i = 0; i < tris.Length; i += 3)
        {
            Vector3 a, b, c;

            a = vertices[tris[i]];
            b = vertices[tris[i + 1]];
            c = vertices[tris[i + 2]];

            // Calculate vertexes resulting from subdivision along edges.
            Vector3[] intervalsAB = CalculateIntervals(a, b, resolution + 1);
            Vector3[] intervalsBC = CalculateIntervals(b, c, resolution + 1);
            Vector3[] intervalsAC = CalculateIntervals(a, c, resolution + 1);

            // Add the vertexes to our list if they're not there already

            List<Vector3> newV3s = new List<Vector3>();
            newV3s.AddRange(intervalsAB);
            newV3s.AddRange(intervalsAC);
            newV3s.AddRange(intervalsBC);

            foreach (Vector3 v in newV3s)
            {
                GetOrAddToIndex(ref vertIndex, ref vertices2, v, ref nextVertex);
            }

            // Add T new triangles using vertex2 indexes

            for (int j = 1; j < intervalsAC.Length; j++)
            {
                // Number of divisions on this row: j - 1
                // Number of edges = j

                // A vector representing a step of a single edge along the plane BC
                Vector3 r = intervalsAB[j] - intervalsAC[j];
                r = r / j;

                for (int k = 0; k < j; k++)
                {
                    // For every edge, we form an upward pointing triangle.
                    // This goes to the point one step "up" the AC edge and k along the BC edge

                    //                                       p
                    //                                     /   \
                    // Define the points of a triangle as s --> t

                    Vector3 s = intervalsAC[j] + (r * k);
                    Vector3 t = s + r;

                    Vector3 pointUp = new Vector3();
                    pointUp = intervalsAC[j - 1] + (r * k);

                    tris2[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices2, s, ref nextVertex);
                    tris2[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices2, pointUp, ref nextVertex);
                    tris2[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices2, t, ref nextVertex);

                    // For all but the last row, do down triangles too
                    if (j < intervalsAB.Length - 1)
                    {
                        Vector3 pointDown = intervalsAC[j + 1] + (r * (k + 1));
                        tris2[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices2, s, ref nextVertex);
                        tris2[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices2, t, ref nextVertex);
                        tris2[nextTri++] = GetOrAddToIndex(ref vertIndex, ref vertices2, pointDown, ref nextVertex);
                    }
                }
            }
        }

        // Project every point onto a sphere of radius r

        for (int i = 0; i < vertices2.Length; i++)
        {
            Vector3 v = vertices2[i];
            float f = v.magnitude / vertices2[0].magnitude;

            vertices2[i] = v / f;
        }

        tris = tris2;
        vertices = vertices2;

        Debug.Log($"{resolution} - {tris.Length / 3} tris, {vertices.Length} vertices");

        UnityEngine.Profiling.Profiler.EndSample();
    }

    /**
     * <summary>Attempts to look up a v3's vertex index in the reference, or adds it to both objects if it's not found. 
     * Returns the index of the Vector3 in the <c>v3s</c> array.</summary>
     */
    private int GetOrAddToIndex(ref Hashtable index, ref Vector3[] v3s, Vector3 v, ref int nextIndex)
    {
        Vector3Int v3i = new Vector3Int((int)Math.Round(v.x * 1000,0), (int)Math.Round(v.y * 1000,0), (int)Math.Round(v.z * 1000, 0));

        if (index[v3i] != null)
        {
            return (int)index[v3i];
        }

        v3s[nextIndex] = v;
        index[v3i] = nextIndex;
        return nextIndex++;
        
    }

    /**
     * <summary>Calculate a list of Vector3 repressenting the length between two Vector3s subdivided <c>interval</c> times.</summary>
     * <param name="a">First point</param> 
     * <param name="b">Second point</param>
     * <param name="interval">Subdivision factor</param>
     */
    private Vector3[] CalculateIntervals(Vector3 a, Vector3 b, int interval)
    {
        if(interval < 1)
        {
            throw new System.ArgumentException("Invalid interval");
        }

        Vector3[] m = new Vector3[interval + 1];

        Vector3 step = (b - a) / interval;

        for (int i = 0; i <= interval; i++)
        {
            m[i] = a + (step * i);
        }

        return m; 
    }


    /**
     *  <summary>Deform the vertices of this icosphere scaled to the <c>smallScaleDeform</c> factor</summary>
     */
    public void Deform()
    {

        UnityEngine.Profiling.Profiler.BeginSample("IcosahedronSphere.Deform");

        float min = (smallScaleDeform / -2.0f);

        for (int i = 0; i < vertices.Length; i++)
        {
            float f = Mathf.PerlinNoise(vertices[i].x - radius, vertices[i].y - radius) * smallScaleDeform + min;
            vertices[i] = vertices[i] * (1.0f + f);
            progressValue = i;
        }
        needsUpdate = true;
        UnityEngine.Profiling.Profiler.EndSample();
    }

    /**
     * <summary>Update the mesh of this Icosphere</summary>
     */
    public void RecalculateMesh()
    {
        m.Clear();
        m.SetVertices(vertices);
        m.SetTriangles(tris,0);
        m.RecalculateNormals();
        m.RecalculateBounds();
        needsUpdate = false;
    }

    //   public void OnDrawGizmosSelected()
    //    {
    //       Mesh m = gameObject.GetComponent<MeshFilter>().sharedMesh;
    //
    //        for (int i = 0; i < m.vertices.Length; i++)
    //        {
    //          Handles.Label(m.vertices[i], i.ToString());
    //        }
    //    }

} 
