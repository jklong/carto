﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * <summary>A coordinate on the Cartesian plane</summary>
 */
public struct Cartesian
{
    public int x;
    public int y;
}

/**
 * <summary>A coordinate describing a point on a sphere. Angles for the coordinate ares in radians.</summary>
 */
public struct Spherical
{
    public double latitude;
    public double longitude;
}
