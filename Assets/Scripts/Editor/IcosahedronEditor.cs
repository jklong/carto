﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
    
[CustomEditor(typeof(IcosahedronSphere))]
public class IcosahedronEditor : Editor
{

    public override void OnInspectorGUI()
    {
        IcosahedronSphere icosphere = (IcosahedronSphere)target;

        DrawDefaultInspector();

        if (GUILayout.Button("Generate"))
        {
            icosphere.Generate();
        }

        if (GUILayout.Button("Deform"))
        {
            icosphere.Deform();
            icosphere.RecalculateMesh();
        }
    }
}
