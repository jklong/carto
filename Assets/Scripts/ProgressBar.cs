﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public void update_label()
    {
        Slider slider = gameObject.GetComponent<Slider>();
        TMPro.TextMeshProUGUI txt = slider.GetComponentInChildren<TMPro.TextMeshProUGUI>();
        if(slider.maxValue == 0)
        {
            txt.text = "";
        } else
        {
             txt.text = Format(slider.value) + " / " + Format(slider.maxValue);
        }
        
    }

    private string Format(float val) {
        return string.Format("{0:##,#}", (int)val);
    }
}
