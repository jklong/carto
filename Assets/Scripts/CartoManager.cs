﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartoManager : MonoBehaviour
{
    public int seed;
    public System.Random rng;
    public IcosahedronSphere globe;

    // Start is called before the first frame update
    void Start()
    {
        if (seed == 0) {
            rng = new System.Random();
        } else
        {
            rng = new System.Random(seed);
        }

        //InitialiseWorld();
    }

    public void InitialiseWorld()
    {
        UnityEngine.Profiling.Profiler.BeginSample("CartoManager.IntialiseWorld");

        GameObject globeGO = globe.gameObject;
        if (!globeGO.activeSelf)
        {
            globeGO.SetActive(true);
        }

        Debug.Log($"Using resolution: {globe.resolution}\nUsing smallScaleDeform: {globe.smallScaleDeform}");
        globe.Generate();

        UnityEngine.Profiling.Profiler.EndSample();
    }

}
