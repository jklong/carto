﻿using System;

/**
 * <summary>Provides transformations between spherical and cartesian coordinates using
 * a Mollweide transform. The returned coordinates are centered around the equator at meridian 0.</summary>
 */
public class Mollweide
{
    const float LAMBDA_0 = 0.0f;
    float width;
    float height;
    double projectionRadius;

    /**
     * <summary>Construct a projection onto a map using a Mollweide transform</summary>
     * <param name="width">the width of the map, in px</param>
     * <param name="height">the height of the map, in px</param>
     */
    public Mollweide(float width, float height)
    {
        this.width = width;
        this.height = height;

        this.projectionRadius = this.width / (2.0d * Math.PI); //Convert to a circumference

    }

}
